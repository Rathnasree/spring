package com.example.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpingDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpingDemoApplication.class, args);
	}

}
